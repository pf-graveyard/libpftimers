#ifndef PFTIMERS_H
#define PFTIMERS_H

/*
    libpftimers — yet another silly and dumb timers implementation
    © 2013–2014, Oleksandr Natalenko aka post-factum <oleksandr@natalenko.name>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <pthread.h>

#define LIBPFTIMERS_MAJOR          0
#define LIBPFTIMERS_MINOR          0
#define LIBPFTIMERS_RELEASE        1
#define LIBPFTIMERS_API            1

#define PFTIMER_ASYNC	1
#define PFTIMER_SYNC	2
#define PFTIMER_ONESHOT	3

#define LIBPFTIMERS_MONITOR_THREAD_NAME "lpftmon"

typedef void (*function_t)(void *);

typedef struct pftimer
{
	pthread_t thread_id;
	function_t function;
	void *function_args;
	unsigned int period, exit_condition, async, delay;
	pthread_mutex_t exit_lock;
	const char *name;
} pftimer_t;

typedef struct pftimers_monitor_message
{
	long mtype;
	pthread_t ptid;
} pftimers_monitor_message_t;

pftimer_t *pftimers_create_async(const char *_name, function_t _tick_function, void *_tick_function_data, unsigned int _ms_period);
pftimer_t *pftimers_create_sync(const char *_name, function_t _tick_function, void *_tick_function_data, unsigned int _ms_period);
pftimer_t *pftimers_defer_async(const char *_name, function_t _tick_function, void *_tick_function_data, unsigned int _ms_period, unsigned int _ms_delay);
pftimer_t *pftimers_defer_sync(const char *_name, function_t _tick_function, void *_tick_function_data, unsigned int _ms_period, unsigned int _ms_delay);
pftimer_t *pftimers_defer_oneshot(const char *_name, function_t _tick_function, void *_tick_function_data, unsigned int _ms_delay);
void pftimers_stop(pftimer_t *_timer);

#endif /* PFTIMERS_H */

