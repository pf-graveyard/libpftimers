/*
    libpftimers — yet another silly and dumb timers implementation
    © 2013–2014, Oleksandr Natalenko aka post-factum <oleksandr@natalenko.name>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>
#include <sysexits.h>
#include <time.h>
#include <unistd.h>
#include "../../libpftimers.h"

#define TIMER1_PERIOD	1000
#define TIMER2_PERIOD	1000
#define TIMER1_DELAY	1000
#define TIMER2_DELAY	2000

signed long long prev1, prev2;

void tick_sync(void *_data)
{
	int *counter = _data;
	(*counter)++;
	struct timespec current_time;
	clock_gettime(CLOCK_REALTIME, &current_time);
	signed long long cur = current_time.tv_sec * 1000000000L + current_time.tv_nsec;
	signed long long drift = cur - prev1 - TIMER1_PERIOD * 1000000L;
	if (prev1 == 0)
		printf("Sync timer, tick #%d at %lu.%lu\n", *counter, current_time.tv_sec, current_time.tv_nsec);
	else
		printf("Sync timer, tick #%d at %lu.%lu, drift: %lld nsecs\n", *counter, current_time.tv_sec, current_time.tv_nsec, drift);
	prev1 = cur;
}

void tick_async(void *_data)
{
	int *counter = _data;
	(*counter)++;
	struct timespec current_time;
	clock_gettime(CLOCK_REALTIME, &current_time);
	signed long long cur = current_time.tv_sec * 1000000000L + current_time.tv_nsec;
	signed long long drift = cur - prev2 - TIMER1_PERIOD * 1000000L;
	if (prev2 == 0)
		printf("\t\tAsync timer, tick #%d at %lu.%lu\n", *counter, current_time.tv_sec, current_time.tv_nsec);
	else
		printf("\t\tAsync timer, tick #%d at %lu.%lu, drift: %lld nsecs\n", *counter, current_time.tv_sec, current_time.tv_nsec, drift);
	prev2 = cur;
}

int main(int argc, char **argv)
{
	(void)argc;
	(void)argv;

	prev1 = 0, prev2 = 0;
	int counter1 = 0, counter2 = 0;

	printf("Creating sync timer with %u ms period...\n", TIMER1_PERIOD);
	pftimer_t *timer1 = pftimers_defer_sync("Sync timer", tick_sync, &counter1, TIMER1_PERIOD, TIMER1_DELAY);
	if (timer1 == NULL)
	{
		fprintf(stderr, "Failed to create timer. Exiting...\n");
		exit(EX_OSERR);
	}
	printf("Created. Thread ID is %lu. Will start in %u msecs.\n", timer1->thread_id, TIMER1_DELAY);

	printf("Creating async timer with %u ms period...\n", TIMER2_PERIOD);
	pftimer_t *timer2 = pftimers_defer_async("Async timer", tick_async, &counter2, TIMER2_PERIOD, TIMER2_DELAY);
	if (timer2 == NULL)
	{
		fprintf(stderr, "Failed to create timer. Exiting...\n");
		exit(EX_OSERR);
	}
	printf("Created. Thread ID is %lu. Will start in %u msecs.\n", timer2->thread_id, TIMER2_DELAY);

	printf("Press Enter to stop all timers...\n");
	getchar();
	printf("Stopping sync timer...\n");
	pftimers_stop(timer1);
	printf("Stopping async timer...\n");
	pftimers_stop(timer2);
	printf("Timers stopped. Exiting...\n");
	exit(EX_OK);
}

