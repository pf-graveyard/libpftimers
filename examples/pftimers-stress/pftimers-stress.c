/*
    libpftimers — yet another silly and dumb timers implementation
    © 2013–2014, Oleksandr Natalenko aka post-factum <oleksandr@natalenko.name>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <sysexits.h>
#include <time.h>
#include <unistd.h>
#include "../../libpftimers.h"

#define TIMER1_PERIOD 1
#define TIMER2_PERIOD 1
#define TIMERM_PERIOD 1000


typedef struct container
{
	unsigned long long counter1, counter2;
	pthread_mutex_t counter1_lock, counter2_lock;
} container_t;

void tick1(void *_data)
{
	container_t *counters = _data;
	pthread_mutex_lock(&counters->counter1_lock);
	counters->counter1++;
	pthread_mutex_unlock(&counters->counter1_lock);
}

void tick2(void *_data)
{
	container_t *counters = _data;
	pthread_mutex_lock(&counters->counter2_lock);
	counters->counter2++;
	pthread_mutex_unlock(&counters->counter2_lock);
}

void tickm(void *_data)
{
	container_t *counters = _data;

	pthread_mutex_lock(&counters->counter1_lock);
	pthread_mutex_lock(&counters->counter2_lock);
	printf("Ticks per second:\tasync: %llu\tsync: %llu\n", counters->counter1, counters->counter2);
	counters->counter1 = 0;
	counters->counter2 = 0;
	pthread_mutex_unlock(&counters->counter1_lock);
	pthread_mutex_unlock(&counters->counter2_lock);
}

int main(int argc, char **argv)
{
	(void)argc;
	(void)argv;

	container_t counters;

	pthread_mutex_init(&counters.counter1_lock, NULL);
	pthread_mutex_init(&counters.counter2_lock, NULL);


	counters.counter1 = 0;
	counters.counter2 = 0;

	printf("Creating async timer with %u ms period...\n", TIMER1_PERIOD);
	pftimer_t *timer1 = pftimers_create_async("Async timer", tick1, &counters, TIMER1_PERIOD);
	if (timer1 == NULL)
	{
		fprintf(stderr, "Failed to create timer. Exiting...\n");
		exit(EX_OSERR);
	}
	printf("Created. Thread ID is %lu.\n", timer1->thread_id);

	printf("Creating sync timer with %u ms period...\n", TIMER2_PERIOD);
	pftimer_t *timer2 = pftimers_create_sync("Sync timer", tick2, &counters, TIMER2_PERIOD);
	if (timer2 == NULL)
	{
		fprintf(stderr, "Failed to create timer. Exiting...\n");
		exit(EX_OSERR);
	}
	printf("Created. Thread ID is %lu.\n", timer2->thread_id);

	printf("Creating monitor timer with %u ms period...\n", TIMERM_PERIOD);
	pftimer_t *timerm = pftimers_defer_async("Monitor timer", tickm, &counters, TIMERM_PERIOD, TIMERM_PERIOD);
	if (timerm == NULL)
	{
		fprintf(stderr, "Failed to create timer. Exiting...\n");
		exit(EX_OSERR);
	}
	printf("Created. Thread ID is %lu.\n", timerm->thread_id);

	printf("Press Enter to stop all timers...\n");
	getchar();
	printf("Stopping counting timers...\n");
	pftimers_stop(timer1);
	pftimers_stop(timer2);
	printf("Stopping monitor timer...\n");
	pftimers_stop(timerm);
	printf("All timers stopped. Exiting...\n");

	pthread_mutex_destroy(&counters.counter1_lock);
	pthread_mutex_destroy(&counters.counter2_lock);

	exit(EX_OK);
}

