/*
    libpftimers — yet another silly and dumb timers implementation
    © 2013–2014, Oleksandr Natalenko aka post-factum <oleksandr@natalenko.name>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <sysexits.h>
#include <time.h>
#include <unistd.h>
#include "../../libpftimers.h"

#define ONESHOT_DELAY 5000


void shot(void *_data)
{
	(void)_data;
	printf("Shot!\n");
}

int main(int argc, char **argv)
{
	(void)argc;
	(void)argv;

	printf("Deferring oneshot for %u ms...\n", ONESHOT_DELAY);
	pftimer_t *shotgun = pftimers_defer_oneshot("Oneshot", shot, NULL, ONESHOT_DELAY);
	if (shotgun == NULL)
	{
		fprintf(stderr, "Failed to create timer. Exiting...\n");
		exit(EX_OSERR);
	}
	printf("Created. Thread ID is %lu. Will shot in %d msecs.\n", shotgun->thread_id, ONESHOT_DELAY);
	printf("Press Enter to free timer resources...\n");
	getchar();
	printf("Freeing...\n");
	pftimers_stop(shotgun);
	printf("All timers stopped. Exiting...\n");

	exit(EX_OK);
}

