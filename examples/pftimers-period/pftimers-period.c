/*
    libpftimers — yet another silly and dumb timers implementation
    © 2013–2014, Oleksandr Natalenko aka post-factum <oleksandr@natalenko.name>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>
#include <sysexits.h>
#include <time.h>
#include <unistd.h>
#include "../../libpftimers.h"

#define TIMER1_PERIOD 1000
#define TIMER2_PERIOD 500
#define TIMER3_PERIOD 250

void tick1(void *_data)
{
	int *counter = _data;
	(*counter)++;
	struct timespec current_time;
	clock_gettime(CLOCK_REALTIME, &current_time);
	printf("Timer 1, tick #%d at %lu.%lu\n", *counter, current_time.tv_sec, current_time.tv_nsec);
}

void tick2(void *_data)
{
	int *counter = _data;
	(*counter)++;
	struct timespec current_time;
	clock_gettime(CLOCK_REALTIME, &current_time);
	printf("\tTimer 2, tick #%d at %lu.%lu\n", *counter, current_time.tv_sec, current_time.tv_nsec);
}

void tick3(void *_data)
{
	int *counter = _data;
	(*counter)++;
	struct timespec current_time;
	clock_gettime(CLOCK_REALTIME, &current_time);
	printf("\t\tTimer 3, tick #%d at %lu.%lu\n", *counter, current_time.tv_sec, current_time.tv_nsec);
}

int main(int argc, char **argv)
{
	(void)argc;
	(void)argv;

	int counter1 = 0;
	printf("Creating timer with %u ms period...\n", TIMER1_PERIOD);
	pftimer_t *timer1 = pftimers_create_async("Tick timer 1", tick1, &counter1, TIMER1_PERIOD);
	if (timer1 == NULL)
	{
		fprintf(stderr, "Failed to create timer. Exiting...\n");
		exit(EX_OSERR);
	}
	printf("Created. Thread ID is %lu.\n", timer1->thread_id);

	int counter2 = 0;
	printf("Creating timer with %u ms period...\n", TIMER2_PERIOD);
	pftimer_t *timer2 = pftimers_create_async("Tick timer 2", tick2, &counter2, TIMER2_PERIOD);
	if (timer2 == NULL)
	{
		fprintf(stderr, "Failed to create timer. Exiting...\n");
		exit(EX_OSERR);
	}
	printf("Created. Thread ID is %lu.\n", timer2->thread_id);

	int counter3 = 0;
	printf("Creating timer with %u ms period...\n", TIMER3_PERIOD);
	pftimer_t *timer3 = pftimers_create_async("Tick timer 3", tick3, &counter3, TIMER3_PERIOD);
	if (timer3 == NULL)
	{
		fprintf(stderr, "Failed to create timer. Exiting...\n");
		exit(EX_OSERR);
	}
	printf("Created. Thread ID is %lu.\n", timer3->thread_id);

	printf("Press Enter to stop all timers...\n");
	getchar();
	printf("Stopping timer #1...\n");
	pftimers_stop(timer1);
	printf("Stopping timer #2...\n");
	pftimers_stop(timer2);
	printf("Stopping timer #3...\n");
	pftimers_stop(timer3);
	printf("All timers stopped. Exiting...\n");
	exit(EX_OK);
}

