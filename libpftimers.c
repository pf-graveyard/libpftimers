/*
    libpftimers — yet another silly and dumb timers implementation
    © 2013–2014, Oleksandr Natalenko aka post-factum <oleksandr@natalenko.name>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <errno.h>
#include <pthread.h>
#include <stdlib.h>
#include <sysexits.h>
#include <time.h>

#include "libpftimers.h"
#include "timetools.h"

static void *pftimers_tick(void *_data)
{
	pftimer_t *data = _data;
	data->function(data->function_args);
	return NULL;
}

static void *pftimers_worker(void *_data)
{
	pftimer_t *data = _data;

	if (pthread_setname_np(pthread_self(), data->name) != 0)
		return NULL;

	if (data->delay != 0)
		pftimers_nssleep(ms_to_ns(data->delay));

	if (data->async == PFTIMER_ONESHOT)
	{
		data->function(data->function_args);
		return NULL;
	}

	unsigned long long sleep_time = ms_to_ns(data->period);

	while (1)
	{
		if (pthread_mutex_lock(&data->exit_lock) != 0)
			break;
		if (data->exit_condition)
		{
			pthread_mutex_unlock(&data->exit_lock);
			break;
		} else if (pthread_mutex_unlock(&data->exit_lock) != 0)
			break;

		if (data->async == PFTIMER_ASYNC)
		{
			pthread_t new_tick_tid;
			if (pthread_create(&new_tick_tid, NULL, pftimers_tick, _data) != 0)
				break;
			pthread_detach(new_tick_tid);
		} else if (data->async == PFTIMER_SYNC)
			data->function(data->function_args);

		pftimers_nssleep(sleep_time);
	}
	return NULL;
}

pftimer_t *pftimers_create(const char *_name, function_t _tick_function, void *_tick_function_data, unsigned int _ms_period, unsigned int _ms_delay, unsigned int _async)
{
	pftimer_t *new_timer = malloc(sizeof(pftimer_t));
	if (new_timer == NULL)
		return NULL;
	new_timer->period = _ms_period;
	new_timer->function = _tick_function;
	new_timer->function_args = _tick_function_data;
	new_timer->async = _async;
	new_timer->delay = _ms_delay;
	new_timer->name = _name;
	if (pthread_mutex_init(&new_timer->exit_lock, NULL) != 0)
	{
		free(new_timer);
		return NULL;
	}
	new_timer->exit_condition = 0;
	if (pthread_create(&new_timer->thread_id, NULL, pftimers_worker, new_timer) != 0)
	{
		free(new_timer);
		return NULL;
	} else
		return new_timer;
}

pftimer_t *pftimers_create_async(const char *_name, function_t _tick_function, void *_tick_function_data, unsigned int _ms_period)
{
	return pftimers_create(_name, _tick_function, _tick_function_data, _ms_period, 0, PFTIMER_ASYNC);
}

pftimer_t *pftimers_create_sync(const char *_name, function_t _tick_function, void *_tick_function_data, unsigned int _ms_period)
{
	return pftimers_create(_name, _tick_function, _tick_function_data, _ms_period, 0, PFTIMER_SYNC);
}

pftimer_t *pftimers_defer_async(const char *_name, function_t _tick_function, void *_tick_function_data, unsigned int _ms_period, unsigned int _ms_delay)
{
	return pftimers_create(_name, _tick_function, _tick_function_data, _ms_period, _ms_delay, PFTIMER_ASYNC);
}

pftimer_t *pftimers_defer_sync(const char *_name, function_t _tick_function, void *_tick_function_data, unsigned int _ms_period, unsigned int _ms_delay)
{
	return pftimers_create(_name, _tick_function, _tick_function_data, _ms_period, _ms_delay, PFTIMER_SYNC);
}

pftimer_t *pftimers_defer_oneshot(const char *_name, function_t _tick_function, void *_tick_function_data, unsigned int _ms_delay)
{
	return pftimers_create(_name, _tick_function, _tick_function_data, 0, _ms_delay, PFTIMER_ONESHOT);
}

void pftimers_stop(pftimer_t *_timer)
{
	if (pthread_mutex_lock(&_timer->exit_lock) != 0)
		return;
	_timer->exit_condition = 1;
	if (pthread_mutex_unlock(&_timer->exit_lock) != 0)
		return;
	if (pthread_join(_timer->thread_id, NULL) != 0)
		return;
	if (pthread_mutex_destroy(&_timer->exit_lock) != 0)
		return;
	free(_timer);
}

__attribute__((constructor)) void init(void)
{

}

__attribute__((destructor)) void done(void)
{

}

