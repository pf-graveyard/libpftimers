/*
    libpftimers — yet another silly and dumb timers implementation
    © 2013–2014, Oleksandr Natalenko aka post-factum <oleksandr@natalenko.name>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <errno.h>
#include <time.h>

#include "timetools.h"

struct timespec ns_to_timespec(unsigned long long _ns)
{
	struct timespec ret;
	unsigned long long seconds = _ns / 1000000000L;
	_ns -= seconds * 1000000000L;
	ret.tv_sec = seconds;
	ret.tv_nsec = _ns;
	return ret;
}

unsigned long long ms_to_ns(unsigned long long _ms)
{
	return _ms * 1000000L;
}

void pftimers_nssleep(unsigned long long _ns)
{
	struct timespec requested_time = ns_to_timespec(_ns);
	while (nanosleep(&requested_time, &requested_time) == -1 && errno == EINTR)
		continue;
}

